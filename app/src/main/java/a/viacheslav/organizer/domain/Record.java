package a.viacheslav.organizer.domain;

import android.os.Parcelable;

public class Record  {      //implements Parcelable

    private int key;
    private String title = "empty";
    private String content = "empty";

    public Record (int key, String title, String content){
        this.key = key;
        this.title = title;
        this.content = content;
    }

    public Record (String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }



}
