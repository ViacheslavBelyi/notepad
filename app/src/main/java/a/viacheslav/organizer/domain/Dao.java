package a.viacheslav.organizer.domain;

import io.reactivex.Observable;

public interface Dao<T> {
    void add(T t);
    void update(T t);
    void remove(T t);

    Observable<T> getAll();

}
