package a.viacheslav.organizer.ui;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import a.viacheslav.organizer.R;
import a.viacheslav.organizer.Util;
import a.viacheslav.organizer.domain.Record;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import static a.viacheslav.organizer.Util.log;

public class MainAdapter extends RecyclerView.Adapter<ViewHolder> {

    private final LayoutInflater inflater ;

    private List<Record> records;

    public MainAdapter(Context context, List<Record> recordList) {
        inflater = LayoutInflater.from(context);
        records = recordList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.record , parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Record record = records.get(position);

        holder.recordTitle.setText(record.getTitle());

        holder.setTag(record);                 //?
    }

    @Override
    public int getItemCount() {
        return records.size();
    }
}

class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.record_title)
    TextView recordTitle;

    private final View holderView;

    ViewHolder(View v){
        super(v);
        ButterKnife.bind(this,v);

        this.holderView = v;
    }

    void setTag(Object object){
        holderView.setTag(object);
    }

}
