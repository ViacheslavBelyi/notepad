package a.viacheslav.organizer.ui;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import a.viacheslav.organizer.R;
import a.viacheslav.organizer.Util;
import a.viacheslav.organizer.data.RecordHelper;
import a.viacheslav.organizer.data.SqlDao;
import a.viacheslav.organizer.domain.Record;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // todo rm
        //fillDb();

    }

    void fillDb(){
        SQLiteDatabase db = new RecordHelper(this).getWritableDatabase();
        SqlDao dao = new SqlDao(db);

        int elems = 10000;

        for (int i = 0; i < elems; i++){
            Record record = new Record(i, "", "");
            dao.remove(record);
        }

        for (int i = 0; i < elems; i++){
            Record record = new Record("title # " + i , "content :" + System.nanoTime());
            dao.add(record);
        }
    }
}



