package a.viacheslav.organizer.ui.fragment;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import a.viacheslav.organizer.R;
import a.viacheslav.organizer.Util;
import a.viacheslav.organizer.data.RecordHelper;
import a.viacheslav.organizer.data.SqlDao;
import a.viacheslav.organizer.domain.Record;
import a.viacheslav.organizer.ui.MainAdapter;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;

public class MainFragment extends Fragment {

    @BindView(R.id.main_recycleView)
    RecyclerView recyclerView;

    @BindView(R.id.add_button)
    FloatingActionButton addButton;

    List<Record> records ;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    long startTime = System.nanoTime();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        records =  new CopyOnWriteArrayList<>();

        SQLiteDatabase db = new RecordHelper(this.getContext()).getWritableDatabase();
        SqlDao dao = new SqlDao(db);

        Observable<Record> recordObservable = dao.getAll();

        compositeDisposable.add(
                recordObservable.subscribe(record -> records.add(record))
        );


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ButterKnife.bind(this, view);


        addButton.setOnClickListener(v ->
                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_editRecordFragment));

        recyclerView.setAdapter(new MainAdapter(view.getContext(), records));

        return view;
    }



    @Override
    public void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

}
