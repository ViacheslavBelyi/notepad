package a.viacheslav.organizer.data;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Looper;

import a.viacheslav.organizer.domain.Dao;
import a.viacheslav.organizer.domain.Record;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;

public class SqlDao implements Dao<Record> {

    private final SQLiteDatabase database;

    public SqlDao(SQLiteDatabase database){
        this.database = database;
    }

    @Override
    public void add(Record record) {

        ContentValues values = new ContentValues();

        values.put(DatabaseSchema.Records.Columns.TITLE, record.getTitle());
        values.put(DatabaseSchema.Records.Columns.CONTENT, record.getContent());

        database.insert(DatabaseSchema.Records.TABLE_NAME, null, values);
    }

    @Override
    public void update(Record record) {

        database.execSQL(" update " + DatabaseSchema.Records.TABLE_NAME+ " set " +
                DatabaseSchema.Records.Columns.TITLE + " = \"" + record.getTitle() + "\" , " +
                DatabaseSchema.Records.Columns.CONTENT + " = \"" + record.getContent() + "\" " +
                "where " + DatabaseSchema.Records.Columns.KEY + " = " + record.getKey() );

    }

    @Override
    public void remove(Record record) {

        database.execSQL(" DELETE FROM " + DatabaseSchema.Records.TABLE_NAME + " " +
                " where " + DatabaseSchema.Records.Columns.KEY + " = " + record.getKey() );
    }

    @Override
    public Observable<Record> getAll() {

        Observable records = ReplaySubject.create(emitter -> {

            RecordCursor cursor = RecordCursor.getInstance(database, DatabaseSchema.Records.TABLE_NAME);

            while ( ! cursor.isAfterLast()) {
                Record record = cursor.getNextRecord();
                emitter.onNext(record);
            }

            cursor.close();

        }).subscribeOn(Schedulers.io());


        return records;
    }


}
