package a.viacheslav.organizer.data;

public class DatabaseSchema {

    public static class Records {

        public static String TABLE_NAME = "records";

        public static class Columns {
            public static String KEY = "key";
            public static String TITLE = "title";
            public static String CONTENT = "content";
        }

    }
}
