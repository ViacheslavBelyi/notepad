package a.viacheslav.organizer.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class RecordHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "records";
    private static final int VERSION = 1;

    public RecordHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" create table " + DatabaseSchema.Records.TABLE_NAME + "( " +
                DatabaseSchema.Records.Columns.KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DatabaseSchema.Records.Columns.TITLE + ", " +
                DatabaseSchema.Records.Columns.CONTENT + " TEXT " +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
