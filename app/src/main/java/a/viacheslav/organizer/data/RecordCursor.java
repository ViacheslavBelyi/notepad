package a.viacheslav.organizer.data;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;

import a.viacheslav.organizer.domain.Record;

class RecordCursor extends CursorWrapper {

    private Cursor cursor;

    public static RecordCursor getInstance(SQLiteDatabase database, String tableName){
        Cursor cursor = database.query(tableName, null, null, null, null, null, null);
        return new RecordCursor(cursor);
    }

    private RecordCursor(Cursor cursor) {
        super(cursor);

        this.cursor = cursor;
        cursor.moveToFirst();
    }

    public Record getNextRecord() {

        Record record = null;

        if (  ! cursor.isAfterLast() ) {

            int key = getKey();
            String title = getTitle();
            String content = getContent();

            record = new Record(key, title, content);

            cursor.moveToNext();
        }

        return record;
    }

    private int getKey(){
        return cursor.getInt(cursor.getColumnIndex(DatabaseSchema.Records.Columns.KEY));
    }

    private String getTitle(){
        return cursor.getString(cursor.getColumnIndex(DatabaseSchema.Records.Columns.TITLE));
    }

    private String getContent(){
        return cursor.getString(cursor.getColumnIndex(DatabaseSchema.Records.Columns.CONTENT));
    }



}
